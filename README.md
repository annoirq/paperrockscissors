# PAPER-ROCK-SCISSORS GAME
This is console version of Paper-Rock-Scissors game, where you can beat computer.
There are 2 versions of game are present.
Classic one (Paper-Rock-Scissors):

![Scheme](images/classic_game.png)

And bonus version (Paper-Rock-Scissors-Lizard-Spock)

![Scheme](images/bonus_game.png)

Rules loaded from property files:

![Scheme](images/rules.png) 

# How to play.
Just follow instructions. You should type numbers.
Gameplay:

![Scheme](images/game.png) 

# Instructions to run.
Option 1: If you already have Jar-file, then you need Java 8 to run it.
Use follow command:
```
java -jar PaperRockScissors.jar
```

Option 2: Clone project. Locally you need Java 8, Maven installed.
 Go to the project root folder:
 ```
 mvn clean install
 ```
Once build will be succeeded:
```
cd target
java -jar PaperRockScissors.jar 
```
Option 3: Clone project. Open with Intellij (or your favorite IDE), Go to Main.java and run main().
IDE in most cases will do everything for you. In this case you could have Java version 8 or above.