package com.imc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum SymbolEnum {
    PAPER(1),
    SCISSORS(2),
    ROCK(3),
    LIZARD(4),
    SPOCK(5);

    private final int id;

    public static SymbolEnum findById(Integer id) {
        return Arrays.stream(SymbolEnum.values())
                .filter(symbol -> symbol.getId() == id)
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }
}
