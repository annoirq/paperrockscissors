package com.imc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ScoreEnum {
    LOSE(0, "You lose in this round!"),
    DRAW(0, "Draw!"),
    WIN(1, "You win in this round!");

    private final int value;
    private final String message;
}
