package com.imc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum GameTypeEnum {
    CLASSIC(1, "WinnersTableClassic.csv", 3, "PAPER(1), SCISSORS(2), ROCK(3)"),
    BONUS(2, "WinnersTableBonus.csv", 5, "PAPER(1), SCISSORS(2), ROCK(3), LIZARD(4), SPOCK(5)");

    private final Integer id;
    private final String fileLocation;
    private final Integer numberOfSymbols;
    private final String message;

    public static GameTypeEnum findById(int id) {
        return Arrays.stream(GameTypeEnum.values())
                .filter(gameType -> gameType.getId().equals(id))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }
}
