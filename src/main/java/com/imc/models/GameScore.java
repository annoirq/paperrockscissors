package com.imc.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

// Mutable model. Should not be shared across threads.
@Getter
@Setter
@AllArgsConstructor
public class GameScore {
    private int humanScore;
    private int computerScore;
}
