package com.imc.models;

import com.imc.enums.ScoreEnum;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value // this annotation will make this class immutable
@Builder
public class ScorePair {
    @NonNull ScoreEnum humanScore;
    @NonNull ScoreEnum computerScore;
    @NonNull String action;
}
