package com.imc.models;

import com.imc.enums.SymbolEnum;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value // this annotation will make this class immutable
@Builder
public class SymbolPair {
    @NonNull SymbolEnum humanSymbol;
    @NonNull SymbolEnum computerSymbol;
}
