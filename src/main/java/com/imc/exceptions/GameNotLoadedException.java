package com.imc.exceptions;

public class GameNotLoadedException extends Exception {
    public GameNotLoadedException() {
    }

    public GameNotLoadedException(Throwable cause) {
        super(cause);
    }
}
