package com.imc;

import com.imc.enums.GameTypeEnum;
import com.imc.enums.SymbolEnum;
import com.imc.models.GameScore;
import com.imc.models.ScorePair;
import com.imc.models.SymbolPair;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

import static com.imc.utils.ScannerUtil.readInputNumberWithRange;

@AllArgsConstructor
@Getter
public class Game {
    private static final String SPLITERATOR = "===================================================";
    private static final String ROUND_MESSAGE = "Round %s%n";
    private static final String CHOOSE_SYMBOL_MESSAGE = "Choose: %s%n";
    private static final String COMPUTER_CHOSE_MESSAGE = "Computer chose: %s%n";
    private static final String HUMAN_CHOSE_MESSAGE = "You chose: %s%n";
    private static final String HUMAN_TOTAL_SCORE_MESSAGE = "You total score is: %s%n";
    private static final String COMPUTER_TOTAL_SCORE_MESSAGE = "Computer total score is: %s%n";
    private static final String END_GAME_WIN_MESSAGE = "Congratulations, John Connor! You beat AI! All humans can sleep well now!";
    private static final String END_GAME_DRAW_MESSAGE = "DRAW!";
    private static final String END_GAME_LOSE_MESSAGE = "Losing is part of the game. Try one more time...";
    private static final String GAME_STARTED_MESSAGE = "Game is started! Let's play!";

    private final GameTypeEnum gameType;
    private final Map<SymbolPair, ScorePair> winnersTable;
    private final int numberOfRounds;
    private final Scanner scanner;
    private final Random random;

    public void play() {
        System.out.println(SPLITERATOR);
        System.out.println(GAME_STARTED_MESSAGE);
        Integer numberOfSymbols = gameType.getNumberOfSymbols();
        GameScore gameScore = new GameScore(0, 0);

        IntStream.rangeClosed(1, numberOfRounds).forEach(roundNumber ->
                playRound(numberOfSymbols, gameScore, roundNumber));

        printEndGameMessage(gameScore);
    }

    private void playRound(Integer numberOfSymbols, GameScore gameScore, int roundNumber) {
        System.out.println(SPLITERATOR);
        System.out.printf(ROUND_MESSAGE, roundNumber);
        System.out.printf(CHOOSE_SYMBOL_MESSAGE, gameType.getMessage());
        SymbolEnum computerSymbol = SymbolEnum.findById(random.nextInt(numberOfSymbols) + 1);
        SymbolEnum humanSymbol = SymbolEnum.findById(readInputNumberWithRange(scanner, 1, numberOfSymbols));

        System.out.printf(COMPUTER_CHOSE_MESSAGE, computerSymbol);
        System.out.printf(HUMAN_CHOSE_MESSAGE, humanSymbol);
        SymbolPair symbolPair = SymbolPair.builder()
                .humanSymbol(humanSymbol)
                .computerSymbol(computerSymbol)
                .build();
        ScorePair scorePair = winnersTable.get(symbolPair);

        System.out.printf("%s %s %s%n", humanSymbol, scorePair.getAction(), computerSymbol);
        System.out.println(scorePair.getHumanScore().getMessage());
        gameScore.setHumanScore(gameScore.getHumanScore() + scorePair.getHumanScore().getValue());
        gameScore.setComputerScore(gameScore.getComputerScore() + scorePair.getComputerScore().getValue());

        System.out.printf(HUMAN_TOTAL_SCORE_MESSAGE, gameScore.getHumanScore());
        System.out.printf(COMPUTER_TOTAL_SCORE_MESSAGE, gameScore.getComputerScore());
    }

    private void printEndGameMessage(GameScore gameScore) {
        System.out.println(SPLITERATOR);
        if (gameScore.getHumanScore() > gameScore.getComputerScore()) {
            System.out.println(END_GAME_WIN_MESSAGE);
        } else if (gameScore.getHumanScore() == gameScore.getComputerScore()) {
            System.out.println(END_GAME_DRAW_MESSAGE);
        } else {
            System.out.println(END_GAME_LOSE_MESSAGE);
        }
    }
}
