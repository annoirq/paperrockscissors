package com.imc;

import com.imc.exceptions.GameNotLoadedException;
import com.imc.utils.GameStarterUtil;

import java.util.Scanner;

public class Main {
    private static final String GAME_WAS_NOT_LOADED_MESSAGE = "Game was not loaded. Please, contact developers!";

    public static void main(String[] args) {
        try {
            Game game = GameStarterUtil.start(new Scanner(System.in));
            game.play();
        } catch (GameNotLoadedException e) {
            System.out.println(GAME_WAS_NOT_LOADED_MESSAGE);
        }
    }
}
