package com.imc.utils;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class ScannerUtil {
    private static final String REENTER_VALUE_MESSAGE = "Please enter valid number. Possible values: (%s)%n";

    private ScannerUtil() {

    }

    public static int readInputNumberWithRange(Scanner scanner, int min, int max) {
        while(true) {
            String text = scanner.next();
            try {
                int value = Integer.parseInt(text);
                if (value < min || value > max) {
                    throw new IllegalArgumentException();
                }

                return value;
            } catch (IllegalArgumentException e) {
                System.out.printf((REENTER_VALUE_MESSAGE),
                        IntStream.rangeClosed(min, max)
                                .boxed()
                                .map(Object::toString)
                                .collect(Collectors.joining(", ")));
            }
        }
    }
}
