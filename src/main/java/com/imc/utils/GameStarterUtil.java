package com.imc.utils;

import com.imc.Game;
import com.imc.enums.GameTypeEnum;
import com.imc.enums.ScoreEnum;
import com.imc.enums.SymbolEnum;
import com.imc.exceptions.GameNotLoadedException;
import com.imc.models.ScorePair;
import com.imc.models.SymbolPair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

import static com.imc.utils.ScannerUtil.readInputNumberWithRange;

public final class GameStarterUtil {
    private static final String WELCOME_MESSAGE = "Welcome to Rock-Paper-Scissors Game!";
    private static final String SPLITERATOR = "===================================================";
    private static final String CHOOSE_GAME_TYPE_CLASSIC_MESSAGE = "To play classic version Rock-Paper-Scissors press (1)";
    private static final String CHOOSE_GAME_TYPE_BONUS_MESSAGE = "To play bonus version Rock-Paper-Scissors-Lizard-Spock (2)";
    private static final String EMPTY_LINE = "";
    private static final String WHITESPACES_REGEX = "\\s+";
    private static final String CHOOSE_NUMBER_OF_ROUNDS_MESSAGE = "How many rounds do you prefer. Choose value from 1 to 10.";
    private static final int MIN_NUMBERS_OF_ROUNDS = 1;
    private static final int MAX_NUMBERS_OF_ROUNDS = 10;

    private GameStarterUtil() {

    }

    public static Game start(Scanner scanner) throws GameNotLoadedException {
        System.out.println(SPLITERATOR);
        System.out.println(WELCOME_MESSAGE);

        System.out.println(SPLITERATOR);
        System.out.println(CHOOSE_GAME_TYPE_CLASSIC_MESSAGE);
        System.out.println(CHOOSE_GAME_TYPE_BONUS_MESSAGE);
        GameTypeEnum gameType = GameTypeEnum.findById(readInputNumberWithRange(scanner, 1, 2));

        System.out.println(SPLITERATOR);
        System.out.println(CHOOSE_NUMBER_OF_ROUNDS_MESSAGE);
        int numberOfRounds = readInputNumberWithRange(scanner, MIN_NUMBERS_OF_ROUNDS, MAX_NUMBERS_OF_ROUNDS);
        Map<SymbolPair, ScorePair> winnersTable = loadWinnersTable(gameType.getFileLocation());
        return new Game(gameType, winnersTable, numberOfRounds, scanner, new Random());
    }

    private static Map<SymbolPair, ScorePair> loadWinnersTable(String fileLocation) throws GameNotLoadedException {
        InputStream inputStream = Game.class.getClassLoader().getResourceAsStream(fileLocation);

        if (inputStream == null) {
            throw new GameNotLoadedException();
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            return br.lines()
                    .skip(1)
                    .filter(line -> !EMPTY_LINE.equals(line))
                    .map(line -> line.split(WHITESPACES_REGEX))
                    .collect(Collectors.toMap(
                            lineParts -> SymbolPair.builder()
                                    .humanSymbol(SymbolEnum.valueOf(lineParts[0]))
                                    .computerSymbol(SymbolEnum.valueOf(lineParts[1]))
                                    .build(),
                            lineParts -> ScorePair.builder()
                                    .humanScore(ScoreEnum.valueOf(lineParts[2]))
                                    .computerScore(ScoreEnum.valueOf(lineParts[3]))
                                    .action(lineParts[4])
                                    .build()
                    ));
        } catch (IOException e) {
            throw new GameNotLoadedException(e);
        }
    }
}
