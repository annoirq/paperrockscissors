package com.imc.utils;

import com.imc.Game;
import com.imc.enums.GameTypeEnum;
import com.imc.enums.ScoreEnum;
import com.imc.enums.SymbolEnum;
import com.imc.models.ScorePair;
import com.imc.models.SymbolPair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameStarterUtilTest {
    private final String GAME_START_MESSAGE = "===================================================\n" +
            "Welcome to Rock-Paper-Scissors Game!\n" +
            "===================================================\n" +
            "To play classic version Rock-Paper-Scissors press (1)\n" +
            "To play bonus version Rock-Paper-Scissors-Lizard-Spock (2)\n" +
            "===================================================\n" +
            "How many rounds do you prefer. Choose value from 1 to 10.";

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Mock
    private Scanner scanner;

    @Test
    public void start_testClassicGameWith2Rounds() throws Exception {
        when(scanner.next()).thenReturn("1").thenReturn("2");
        Game game = GameStarterUtil.start(scanner);

        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(GAME_START_MESSAGE);
        assertThat(game.getGameType()).isEqualTo(GameTypeEnum.CLASSIC);
        assertThat(game.getNumberOfRounds()).isEqualTo(2);
        assertThat(game.getScanner()).isEqualTo(scanner);

        assertThat(game.getWinnersTable()).hasSize(9);
        assertThat(getSymbolPair(game.getWinnersTable(), SymbolEnum.PAPER, SymbolEnum.ROCK))
            .isEqualTo(getScorePair(ScoreEnum.WIN, ScoreEnum.LOSE, "WRAPS"));
    }

    @Test
    public void start_testBonusGameWith3Rounds() throws Exception {
        when(scanner.next()).thenReturn("2").thenReturn("3");
        Game game = GameStarterUtil.start(scanner);

        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(GAME_START_MESSAGE);
        assertThat(game.getGameType()).isEqualTo(GameTypeEnum.BONUS);
        assertThat(game.getNumberOfRounds()).isEqualTo(3);
        assertThat(game.getScanner()).isEqualTo(scanner);

        assertThat(game.getWinnersTable()).hasSize(25);
        assertThat(getSymbolPair(game.getWinnersTable(), SymbolEnum.SPOCK, SymbolEnum.LIZARD))
            .isEqualTo(getScorePair(ScoreEnum.LOSE, ScoreEnum.WIN, "IS_POISONED_BY"));
    }

    private ScorePair getSymbolPair(Map<SymbolPair, ScorePair> winnersTable, SymbolEnum humanSymbol, SymbolEnum computerSymbol) {
        return winnersTable.get(SymbolPair.builder()
                .humanSymbol(humanSymbol)
                .computerSymbol(computerSymbol)
                .build());
    }

    private ScorePair getScorePair(ScoreEnum humanScore, ScoreEnum computerScore, String action) {
        return ScorePair.builder()
                .humanScore(humanScore)
                .computerScore(computerScore)
                .action(action)
                .build();
    }
}