package com.imc.utils;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static com.imc.utils.ScannerUtil.readInputNumberWithRange;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScannerUtilTest {
    private static final int MIN_INPUT = 1;
    private static final int MAX_INPUT = 5;
    private static final String MESSAGE = "Please enter valid number. Possible values: (1, 2, 3, 4, 5)";
    private static final String INVALID_TEXT = "INVALID_TEXT";
    private static final String OUT_OF_RANGE_NUMBER = "50";

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Mock
    private Scanner scanner;

    @Test
    public void readInputNumberWithRange_ShouldPrintMessageTwice_WhenInputWasIncorrectlySetTwice() {
        when(scanner.next())
                .thenReturn(INVALID_TEXT)
                .thenReturn(OUT_OF_RANGE_NUMBER)
                .thenReturn("3");

        readInputNumberWithRange(scanner, MIN_INPUT, MAX_INPUT);
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(MESSAGE + "\n" + MESSAGE);
    }

    @Test
    public void readInputNumberWithRange_ShouldNotPrintAnything_WhenInputIsMinimumAllowed() {
        when(scanner.next()).thenReturn(String.valueOf(MIN_INPUT));
        readInputNumberWithRange(scanner, MIN_INPUT, MAX_INPUT);
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo("");
    }

    @Test
    public void readInputNumberWithRange_ShouldNotPrintAnything_WhenInputIsMaximumAllowed() {
        when(scanner.next()).thenReturn(String.valueOf(MAX_INPUT));
        readInputNumberWithRange(scanner, MIN_INPUT, MAX_INPUT);
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo("");
    }
}