package com.imc.enums;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SymbolEnumTest {

    @Test
    public void findById_ShouldReturnValidSymbol_WhenIdIsInTheValidRange() {
        assertThat(SymbolEnum.findById(1)).isEqualTo(SymbolEnum.PAPER);
        assertThat(SymbolEnum.findById(2)).isEqualTo(SymbolEnum.SCISSORS);
        assertThat(SymbolEnum.findById(3)).isEqualTo(SymbolEnum.ROCK);
        assertThat(SymbolEnum.findById(4)).isEqualTo(SymbolEnum.LIZARD);
        assertThat(SymbolEnum.findById(5)).isEqualTo(SymbolEnum.SPOCK);
    }

    @Test (expected = IllegalArgumentException.class)
    public void findById_ShouldThrowIllegalArgumentException_WhenIdIsNotInTheValidRange() {
        SymbolEnum.findById(6);
    }
}