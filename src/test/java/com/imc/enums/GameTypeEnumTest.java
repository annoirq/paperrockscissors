package com.imc.enums;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GameTypeEnumTest {

    @Test
    public void findById_ShouldReturnValidGameType_WhenIdIsInTheValidRange() {
        assertThat(GameTypeEnum.findById(1)).isEqualTo(GameTypeEnum.CLASSIC);
        assertThat(GameTypeEnum.findById(2)).isEqualTo(GameTypeEnum.BONUS);
    }

    @Test (expected = IllegalArgumentException.class)
    public void findById_ShouldThrowIllegalArgumentException_WhenIdIsNotInTheValidRange() {
        GameTypeEnum.findById(3);
    }
}