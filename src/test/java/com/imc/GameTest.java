package com.imc;

import com.imc.enums.GameTypeEnum;
import com.imc.enums.ScoreEnum;
import com.imc.enums.SymbolEnum;
import com.imc.models.ScorePair;
import com.imc.models.SymbolPair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {
    private final String CLASSIC_GAME_MESSAGE = "===================================================\n" +
            "Game is started! Let's play!\n" +
            "===================================================\n" +
            "Round 1\n" +
            "Choose: PAPER(1), SCISSORS(2), ROCK(3)\n" +
            "Computer chose: ROCK\n" +
            "You chose: PAPER\n" +
            "PAPER WRAPS ROCK\n" +
            "You win in this round!\n" +
            "You total score is: 1\n" +
            "Computer total score is: 0\n" +
            "===================================================\n" +
            "Congratulations, John Connor! You beat AI! All humans can sleep well now!";

    private final String BONUS_GAME_MSSAGE = "===================================================\n" +
            "Game is started! Let's play!\n" +
            "===================================================\n" +
            "Round 1\n" +
            "Choose: PAPER(1), SCISSORS(2), ROCK(3), LIZARD(4), SPOCK(5)\n" +
            "Computer chose: LIZARD\n" +
            "You chose: SPOCK\n" +
            "SPOCK IS_POISONED_BY LIZARD\n" +
            "You lose in this round!\n" +
            "You total score is: 0\n" +
            "Computer total score is: 1\n" +
            "===================================================\n" +
            "Losing is part of the game. Try one more time...";

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Mock
    private Random random;

    @Mock
    private Scanner scanner;

    @Test
    public void play_ClassicGame() {
        when(scanner.next()).thenReturn("1");
        when(random.nextInt(GameTypeEnum.CLASSIC.getNumberOfSymbols())).thenReturn(2);
        Game game = new Game(GameTypeEnum.CLASSIC, getClassicWinnerTable(), 1, scanner, random);
        game.play();

        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(CLASSIC_GAME_MESSAGE);
    }

    @Test
    public void play_BonusGame() {
        when(scanner.next()).thenReturn("5");
        when(random.nextInt(GameTypeEnum.BONUS.getNumberOfSymbols())).thenReturn(3);
        Game game = new Game(GameTypeEnum.BONUS, getBonusWinnerTable(), 1, scanner, random);
        game.play();

        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(BONUS_GAME_MSSAGE);
    }

    private Map<SymbolPair, ScorePair> getClassicWinnerTable() {
        Map<SymbolPair, ScorePair> winnersTable = new HashMap<>();
        winnersTable.put(
                getSymbolPair(SymbolEnum.PAPER, SymbolEnum.ROCK),
                getScorePair(ScoreEnum.WIN, ScoreEnum.LOSE, "WRAPS")
        );

        return winnersTable;
    }

    private Map<SymbolPair, ScorePair> getBonusWinnerTable() {
        Map<SymbolPair, ScorePair> winnersTable = new HashMap<>();
        winnersTable.put(
                getSymbolPair(SymbolEnum.SPOCK, SymbolEnum.LIZARD),
                getScorePair(ScoreEnum.LOSE, ScoreEnum.WIN, "IS_POISONED_BY")
        );

        return winnersTable;
    }

    private SymbolPair getSymbolPair(SymbolEnum humanSymbol, SymbolEnum computerSymbol) {
        return SymbolPair.builder()
                .humanSymbol(humanSymbol)
                .computerSymbol(computerSymbol)
                .build();
    }

    private ScorePair getScorePair(ScoreEnum humanScore, ScoreEnum computerScore, String action) {
        return ScorePair.builder()
                .humanScore(humanScore)
                .computerScore(computerScore)
                .action(action)
                .build();
    }
}